# express-quickstart

This is a basic project that sets up a basic express project

## Dependences

|Dependency|Installed Version|Use|
|----------|-----------------|---|
|`body-parser`|`^1.18.3`|Used for the parsing of incoming requests|
|`colors`|`^1.3.2`|Used for colouring the output of log messages|
|`compression`|`^1.7.3`|Used for the compression of static files|
|`express`|`^4.16.3`|Used for base webserver implementation|
|`uuid`|`^3.3.2`|Used to tag the incoming requests|

Total number of packages: `115`

## Folder Structure

```
*
+-- libraries/
|   +-- log.js
+-- load/
|   +-- .ignore
|   +-- template.js
+-- node_modules/
+-- static/
+-- .gitignore
+-- begin.py
+-- index.js
+-- package.json
+-- package-lock.json
+-- quickstart.json
+-- README.md
+-- begin.py
```

|Name|Type|Description|
|----|----|-----------|
|`libraries/log.js`|File|The default logging library. This is included by index.js and passed to all loaded element. Provides `log`, `trace`, `debug`, `info`, `warn`, `error` and `fatal` functions which will produce an output with type, time and specific colours by level|
|`load/`|Folder|Contains JS files which will be loaded automatically by `index.js` as long as they are not included in `.ignore` (see below). JS files must export a function taking at least one parameter (the system will giving a warning if something is wrong)|
|`load/.ignore`|File|Contains a list of regex patterns for files that should not be included by `index.js` so a line with `(.*)\.skipped\.js` on it will mean that any file ending in `.skipped.js` will not be included|
|`load/template.js`|File|A template for how each JS file in this folder should be defined. It also includes the basic JSDoc for the log object that is passed if you need it for type hinting. This is not deleted by default|
|`begin.py`|File|The file used to setup the system, this should be run at the start of a project|
|`quickstart.json`|File|Contains the configuration for the launching of the server. See the configuration section|

## Setup

When you are ready to get started, run `begin.py` which will perform the following actions:
1. Delete `package.json` and `package-lock.json`
1. Ask for project details (similar to `npm init` format)
1. Create new `package.json` file with the required dependencies already added
1. Run the command `npm install` (output is routed to stdout and sterr so you see the output)
1. Delete `README.md`, `.git/` and `begin.py` ready for production

## Configuration

This is the schema of the configuration file written in `js-schema` (but the configuration file is not verified using it)

```json
{
    "?https": Boolean,
    "?ssl": {
        "key": String,
        "cert": String,
        "?port": Number,
        "?message": String,
    },
    "?port": Number,
    "?message": String
}
```

|Path|Type|Optional|Description|Default|
|----|----|--------|-----------|-------|
|`$.https`|Boolean|[x]|Whether HTTPS should be enabled, if true SSL must be specified and valid.|False|
|`$.ssl`|Object|[x]|Must be specified if `$.https` is true and must contain all the shown, non-optional field|None|
|`$.ssl.key`|String|[ ]|The file location of the ssl key file|None|
|`$.ssl.cert`|String|[  ]|The file location of the ssl cert file|None|
|`$.ssl.port`|Number|[x]|The port on which the https server should be launched|443|
|`$.ssl.message`|String|[x]|The message to be printed when the HTTPS server launches|`"https server launched"`|
|`$.port`|Number|[x]|The port on which the standard HTTP server should be launched|80|
|`$.message`|String|[x]|The message to be printed when the HTTP server launches|`"http launched successfully"`|

If the file does not exist, a warning will be printed but the server will still launch with just a HTTP server on port 80.

### HTTP and HTTPS
If the HTTPS server is enabled then the HTTPS server will use the express object and the HTTP server will just redirect requests to the HTTPS server.