import os
import subprocess
import sys
import shutil

default_test_command = "echo \\\"Error: no test specified\\\" && exit 1"
package_template = """
{{
  "name": "{}",
  "version": "{}",
  "description": "{}",
  "main": "index.js",
  "scripts": {{
    "test": "{}"
  }},
  {}
  "author": "{}",
  "license": "{}",
  "dependencies": {{
    "body-parser": "^1.18.3",
    "colors": "^1.3.2",
    "compression": "^1.7.3",
    "express": "^4.16.3",
    "uuid": "^3.3.2"
  }}
}}
"""
# "repository": "{}",
#"author": "{}",

print("Begin NPM Init")
print("See `npm help json` for actual info or run `npm init` for the real setup this is based off")
print("This is not as intelligent as npm so repositories, bugs and homepages may not be populated right")
print()

package_name = os.path.basename(os.getcwd())

pn_input = input("package name: ({}) ".format(package_name))
if pn_input != "":
	package_name = pn_input

version = "1.0.0"
v_input = input("version: ({}) ".format(version))
if v_input != "":
	version = v_input

description = input("description: ")

test_command = default_test_command
tc_input = input("test command: ")
if tc_input != "":
	test_command = tc_input

git_repo = None
gr_input = input("git repository: ")
if gr_input != "":
	git_repo = gr_input

keywords = None
kw_input = input("keywords: ")
if kw_input != "":
	keywords = kw_input

author = None
a_input = input("author: ")
if a_input != "":
	author = a_input

license = "ISC"
l_input = input("license: ({}) ".format(license))
if l_input != "":
	license = l_input

repo_author = ""
if keywords is not None:
	keys = list(map(lambda x: x.strip(), keywords.split(",")))
	repo_author += "\"keywords\": ["
	for k in keys:
		repo_author += "\n    \"" + k + "\","
	repo_author = repo_author[:-1]
	repo_author += "\n  ],"
if git_repo is not None:
	repo_author += "\"repository\": \"{}\",\n".format(git_repo)

final_config = package_template.format(package_name, version, description, test_command, repo_author, author, license)

print("Assuming the config is okay, if you have made a mistake, press Ctrl+C and rerun the script")
print()
print(final_config)
print()
# print(os.path.join(os.getcwd(), "package.json"))

package_json_path = os.path.join(os.getcwd(), "package.json")
if os.path.isfile(package_json_path):
	print("(deleting current package.json)")
	os.remove(package_json_path)

print("(writing package.json)")
with open(package_json_path, "w", encoding="utf8") as f:
	f.write(final_config)
	
package_lock_json_path = os.path.join(os.getcwd(), "package-lock.json")
if os.path.isfile(package_lock_json_path):
	print("(deleting current package-lock.json)")
	os.remove(package_lock_json_path)

node_mdules_path = os.path.join(os.getcwd(), "node_modules")
if os.path.isdir(node_mdules_path):
	print("(removing node_modules)")
	shutil.rmtree(node_mdules_path)

print("(spawning `npm install` command)")
print()
process = subprocess.Popen(["cmd", "/C", "npm", "install"], stdout=sys.stdout, stderr=sys.stderr)
process.communicate()
print()

try:
	readme_path = os.path.join(os.getcwd(), "README.md")
	if os.path.isfile(readme_path):
		print("(removing README.md)")
		os.remove(readme_path)
except:
	print("Failed to remove README.md")
	print("You will need to delete this file manually")


try:
	git_path = os.path.join(os.getcwd(), ".git")
	if os.path.isdir(git_path):
		print("(removing .git)")
		shutil.rmtree(git_path)
except:
	print("Failed to remove .git folder")
	print("You will need to delete this folder manually")

print()
print("Setup almost complete, everything is in place and ready, all that is left is to delete this file.")
print("If this fails you will need to delete this file manually")
print()

os.remove(sys.argv[0])

# process = subprocess.Popen(["npm", "install"], stdout=sys.stdout, sterr=sys.stderr)