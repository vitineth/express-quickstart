const path = require('path');
const fs = require('fs');
const express = require('express');
const log = require(path.join(__dirname, 'libraries', 'log.js'));
const parser = require('body-parser');

const compression = require('compression');

const https = require('https');
const http = require('http');

/**
 * The express app from which the base server is run
 * @type {Function|*}
 */
const app = express();

// Load the JSON parser
app.use(parser.json());
// Load the static resource compression
app.use(compression());

// TODO [include static resources]
app.use('/s', express.static(path.join(__dirname, 'static')));

/**
 * Returns a list of named parameters of the given function if the given value is a function, otherwise `undefined`.
 * Code from https://stackoverflow.com/a/9924463
 * @param {function} func the function from which to fetch parameters
 * @return {string|RegExpMatchArray} the list of parameters
 */
function getParamNames(func) {
    if (typeof(func) !== 'function') return undefined;

    const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    const ARGUMENT_NAMES = /([^\s,]+)/g;

    let fnStr = func.toString().replace(STRIP_COMMENTS, '');
    let result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
    if (result === null)
        result = [];
    return result;
}

/**
 * Stores all of the additional properties that should be passed to the load files.
 * @type {Array}
 */
let additionalInjections = [log];

fs.readdir(path.join(__dirname, 'load'),
    /**
     * @param {string} err
     * @param {Array} files
     */
    (err, files) => {
        if (err) {
            log.error('[load]: Failed to read the director at ./load');
            console.error(err);
            return;
        }

        /**
         * Contains the list of patterns that should be excluded from the loading procedure.
         * If a file matches, it will be skipped in the ./load folder
         * @type {Array}
         */
        let ignoredPatterns = [];
        // This is only relevant if we have an .ignore file, if we do then load it, split it on new line each of which
        // represents a pattern and compile them and store them
        if (files.includes('.ignore')) {
            let contents = fs.readFileSync(path.join(__dirname, 'load', '.ignore'), {encoding: 'utf8'}).split('\n');
            // Remove any empty strings so we don't have patterns that match anything and any comments
            contents = contents.filter((value) => value.trim() !== '').filter((value) => value.length > 0 && value.trim().charAt(0) !== '#');
            for (let line of contents) {
                line = line.trim();

                ignoredPatterns.push(new RegExp(line));
            }
            files.splice(files.indexOf('.ignore'), 1);
        }

        /**
         * Test the given value against all of the patterns, returns true on any match, otherwise false
         * @param {string} value
         * @param {[RegExp]} patterns
         */
        function test(value, patterns) {
            for (let pattern of patterns) {
                if (pattern.test(value)) return true;
            }
            return false;
        }

        // Filter out all non-js files
        files = files.filter((value) => value.includes('.') && value.substr(value.lastIndexOf('.')) === '.js');
        files = files.filter((value) => !test(value, ignoredPatterns));

        for (let file of files) {
            // Include the file, all files in the load folder should export a function so verify that first
            let include = require(path.join(__dirname, 'load', file));
            if (typeof(include) !== 'function') {
                log.warn('[load]: File ' + file + ' does not return the correct type. It should export a function not \'' + typeof(include) + '\'');
                continue;
            }

            // Get all the parameters and check that there are enough (it must have the express parameter
            let parameters = getParamNames(include);
            if (parameters.length < 1) {
                log.warn('[load]: File ' + file + ' does not have enough parameters. It must have at least one for the \'express\' object');
                continue;
            }

            // Check if it has too many functions, this is just a utility as it warns that certain parameters will
            // always be undefined as we don't have enough parameters to fill them
            if (parameters.length > additionalInjections.length + 1) {
                parameters.splice(0, additionalInjections.length + 1);
                log.warn('[load]: File ' + file + ' has too many parameters even with additional injections. Params \'' + parameters + '\' will always be undefined');
            }

            // Apply the express app concatenated with the additional parameters to the function
            include.apply(this, [app].concat(additionalInjections));
            log.info('[load]: File ' + file + ' has been successfully required and executed');
        }

        launch();
    });

let launch = () => {
    fs.readFile(path.join(__dirname, 'quickstart.json'), {encoding: 'utf8'}, (err, data) => {
        if (err) {
            // If config isn't present we just launch the server normally on port 80 without https
            log.warn('Failed to load the configuration file. The server will launch with basic setup');
            log.warn('https = disabled');
            log.warn('port = 80');

            app.listen(80, () => log.info('Server launched successfully'));
            return;
        }

        // Load from the config file
        let useHttps = false;
        let port = 80;
        let message = 'http launched successfully';
        let ssl = {key: undefined, cert: undefined, port: 443, message: 'https server launched'};

        let json = JSON.parse(data);
        if (json.hasOwnProperty('https') && json['https'] === true) {
            if (json.hasOwnProperty('ssl') && json['ssl'].hasOwnProperty('key') && json['ssl'].hasOwnProperty('cert')) {
                useHttps = true;

                ssl.key = json['ssl']['key'];
                ssl.cert = json['ssl']['cert'];

                if (json['ssl'].hasOwnProperty('port')) {
                    ssl.port = json['ssl']['port'];
                }

                if (json['ssl'].hasOwnProperty('message')) {
                    ssl.message = json['ssl']['message'];
                }
            }
        }

        if (json.hasOwnProperty('port')) {
            port = json['port'];
        }

        if (json.hasOwnProperty('message')) {
            message = json['message'];
        }

        // If it is https we launch a http server that redirect requests to the https side and then launch the express
        // app with the given security details.
        // If it is http we just launch normally on the given port
        if (useHttps) {
            https.createServer(ssl, app).listen(ssl.port, () => {
                log.info(ssl.message);
                log.info('access on https://localhost' + (ssl.port === 443 ? '' : ':' + ssl.port) + '/');
            });
            http.createServer((req, res) => {
                if (!req.secure) {
                    res.writeHead(301, {
                        'Location': 'https://' + req.headers['host'] + req.url,
                    });
                    res.end();
                }
            }).listen(port, () => {
                log.info(message);
                log.info('access on http://localhost' + (port === 80 ? '' : ':' + port) + '/');
            });
        } else {
            app.listen(port, () => {
                log.info(message);
                log.info('access on http://localhost' + (port === 80 ? '' : ':' + port) + '/');
            });
        }
    })
};