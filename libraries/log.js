/**
 * a logger utility module which prints logs with their file names and line numbers at varying levels
 * @module
 */

const colors = require('colors');

/**
 * Converts the current time into a YYYY-MM-DD HH:mm:ss string
 * @return {string} the formatted date time
 */
function time() {
    let date = new Date();

    return date.getFullYear() + '-' + String(date.getMonth() + 1).padStart(2, '0') + '-' + String(date.getDate()).padStart(2, '0') + ' ' + String(date.getHours()).padStart(2, '0') + ':' + String(date.getMinutes()).padStart(2, '0') + ':' + String(date.getSeconds()).padStart(2, '0');
}

/**
 * The hidden internal log function which prints the message at the given level using the given colouring coloring callback. The origin of the log command is determined in thist function.
 * <br><br>
 * <ol>
 * <li>Generate a new error</li>
 * <li>Get the stacktrace, divide into lines and clean up each line (trip + remove 'at ')</li>
 * <li>Remove the first line (the error type leaving only the real trace)</li>
 * <li>Get the third line in the trace. The first will be this function, the second will be the log function below that called it and the third should (in theory) be the function that called the log function.</li>
 * <li>Extract the file name from the line attempting to one parent</li>
 * <li>Remove the character number from the file name</li>
 * <li>Print out the log message in the format ([time][origin][level] message) passing it through the colouring callback</li>
 * <li>Use NODE_ENV to determine if the program is running in a production environment</li>
 * <li>Insert the log message into the database</li>
 * </ol>
 * @param {string} level the log level that the message should be printed with
 * @param {string} message the real log message
 * @param {function} coloringCallback=(e)=>e a function which applies a colouring effect to the string. Defaults to an arrow function which has no change
 * @param {number} targetIndex=2 the number of steps back to take in the stack trace to find the origin
 */
function internalLog(level, message, coloringCallback = (e) => e, targetIndex = 2) {
    let lines = new Error().stack.split('\n').map((e) => e.trim()).map((e) => e.startsWith('at ') ? e.substr(3) : e);
    lines.shift();
    lines = lines.map((e) => e.substr(e.indexOf('(') + 1)).map((e) => e.substr(0, e.length - 1));
    let target = lines[targetIndex];
    let slashCount = (target.split('/').length - 1) + (target.split('\\').length - 1);
    let result = '';
    if (slashCount >= 2) {
        let encounteredSlashes = 0;
        for (let i = target.length - 1; i >= 0; i--) {
            if (target[i] === '/' || target[i] === '\\') encounteredSlashes++;
            if (encounteredSlashes === 2) {
                result = target.substr(i + 1);
                break;
            }
        }
    } else {
        result = target;
    }

    let origin = result.substr(0, result.lastIndexOf(':'));
    console.log(coloringCallback('[' + time() + '][' + origin + '][' + level + '] ' + message));
}

/**
 * Alias for {@link .internalLog} using the level <code>UNDEF</code> and no color
 *
 * @param {string} message the message to log
 * @param {number} targetIndex=2 the number of steps back to take in the stack trace to find the origin
 */
exports.log = (message, targetIndex=2) => {
    internalLog('UNDEF', message, (e) => e, targetIndex);
};
/**
 * Alias for {@link .internalLog} using the level <code>TRACE</code> and green foreground color
 *
 * @param {string} message the message to log
 * @param {number} targetIndex=2 the number of steps back to take in the stack trace to find the origin
 */
exports.trace = (message, targetIndex=2) => {
    internalLog('TRACE', message, colors.green, targetIndex);
};
/**
 * Alias for {@link .internalLog} using the level <code>DEBUG</code> and magenta foreground color
 *
 * @param {string} message the message to log
 * @param {number} targetIndex=2 the number of steps back to take in the stack trace to find the origin
 */
exports.debug = (message, targetIndex=2) => {
    internalLog('DEBUG', message, colors.magenta, targetIndex);
};
/**
 * Alias for {@link .internalLog} using the level <code>INFO</code> and no color
 *
 * @param {string} message the message to log
 * @param {number} targetIndex=2 the number of steps back to take in the stack trace to find the origin
 */
exports.info = (message, targetIndex=2) => {
    internalLog('INFO', message, (e) => e, targetIndex);
};
/**
 * Alias for {@link .internalLog} using the level <code>WARN</code> and yellow foreground color
 *
 * @param {string} message the message to log
 * @param {number} targetIndex=2 the number of steps back to take in the stack trace to find the origin
 */
exports.warn = (message, targetIndex=2) => {
    internalLog('WARN', message, colors.yellow, targetIndex);
};
/**
 * Alias for {@link .internalLog} using the level <code>ERROR</code> and red foreground color
 *
 * @param {string} message the message to log
 * @param {number} targetIndex=2 the number of steps back to take in the stack trace to find the origin
 */
exports.error = (message, targetIndex=2) => {
    internalLog('ERROR', message, colors.red, targetIndex);
};
/**
 * Alias for {@link .internalLog} using the level <code>FATAL</code> and red background color
 *
 * @param {string} message the message to log
 * @param {number} targetIndex=2 the number of steps back to take in the stack trace to find the origin
 */
exports.fatal = (message, targetIndex=2) => {
    internalLog('FATAL', message, colors.bgRed, targetIndex);
};
